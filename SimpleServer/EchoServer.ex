defmodule EchoServer do
  require Logger

  def accept(port) do
    {:ok, socket} = :gen_tcp.listen(port,
      [:binary, packet: :line, active: false, reuseaddr: true])
    Logger.info "Accepting connections on port #{port}"
    loop_acceptor(socket)
  end

  defp loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    Task.start_link(fn -> serve(client) end)
    loop_acceptor(socket)
  end

  defp serve(socket) do
    socket |> read_line() |> write_line(socket)
    :ok = :gen_tcp.close(socket)
  end

  defp read_line(socket) do
    {:ok, data} = :gen_tcp.recv(socket, 0)

    if (length (String.split(data))) != 3 do
      exit(:invalid_request_format)
    else
      String.split(data, " ")
    end

  end

  defp write_line(line, socket) do
    [request, filename, version] = line

    if request != "GET" do
      message = String.trim(version, "\r\n") <> " 501 Not Implemented"
      :gen_tcp.send(socket, message)
    
    else
      {status, file} = File.open(filename <> ".html")
      
      if status === :ok do
        header = String.trim(version, "\r\n") <>
        " 200 OK\r\n" <>
        "Content-Type\: text/plain\r\n" <>
        "Host localhost\: 9999\r\n" <>
        "Connection\: close\r\n"
        
        content = IO.read(file, :all)
        :ok = File.close(file)
        :gen_tcp.send(socket, header <> content)
      
      else
        message = "HTTP 404\r\n" <>
                  "File not found"
        :gen_tcp.send(socket, message)
      end
    end

  end

  def main(args \\ []) do
    accept(9999)
  end
end